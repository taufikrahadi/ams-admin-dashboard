import { Button, Grid, TableCell } from '@mui/material'
import { DeleteOutline, Pencil } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import React, { useCallback, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'
import Loading from 'src/views/Loading'

const columns: ITableColumn[] = [
  {
    name: 'no',
    title: 'No',
    align: 'left',
    component: ''
  },
  {
    name: 'title',
    title: 'Judul',
    align: 'left',
    component: ''
  },
  {
    name: 'is_active',
    title: 'Status',
    align: 'center',
    component: '',
    formatter(value) {
      return value ? 'AKTIF' : 'TIDAK AKTIF'
    }
  },
  {
    name: 'action',
    title: 'Aksi',
    component: '',
    align: 'center'
  }
]

function NewsIndex() {
  const [loading, startLoading, stopLoading] = useLoading(true)
  const [datas, setDatas] = useState([])
  const router = useRouter()
  useGuard()

  const fetchData = useCallback(() => {
    return supabase
      .from('news')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)
        setDatas(data as any)
      })
  }, [])

  const deleteData = useCallback((id: string) => {
    return supabase.from('news').delete().eq('id', id)
  }, [])


  return (
    <CrudLayout
      columns={columns}
      datas={datas}
      fetchData={fetchData}
      title="News"
      deleteData={deleteData}
      actionChildren={(row, setOpenDialog, setSelectedId) => {
        return (
          <TableCell align="center">
            <Button
              style={{ marginRight: '1rem' }}
              color="warning"
              variant="outlined"
              onClick={() => router.push(`/master/news/edit/${row.id}`)}
            >
              <Pencil />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                setSelectedId(row.id)
                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default NewsIndex
