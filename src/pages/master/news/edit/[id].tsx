import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import {
  IsBoolean,
  IsNotEmpty,
  IsString,
  isNotEmpty,
  isURL
} from 'class-validator'
import { useRouter } from 'next/router'
import { decode } from 'base64-arraybuffer'
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react'
import TextArea from 'src/@core/components/text-area/TextArea'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'
import { Editor } from '@tinymce/tinymce-react'

export class NewsFormSchema {
  @IsString()
  @IsNotEmpty()
  title: string
  banner: string

  @IsString()
  @IsNotEmpty()
  body: string

  @IsBoolean()
  is_active: boolean

  @IsString()
  @IsNotEmpty()
  description: string
}

function EditNews() {
  const [loading, startLoading, stopLoading] = useLoading(true)
  const [isSuccess, setIsSuccess] = useState(false)
  const [isError, setIsError] = useState(false)
  const [form, setForm] = useState<NewsFormSchema>({
    title: '',
    body: '',
    banner: '',
    is_active: true,
    description: ''
  })
  const { errors, validate } = useForm()
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })
  const router = useRouter()
  const { id } = router.query

  const handleChange = useHandleChange(setForm, form)

  const fetchData = useCallback(() => {
    return supabase
      .from('news')
      .select('*')
      .eq('id', id)
      .single()
      .then(({ data, error }) => {
        console.log(data)
        if (error) router.push(`/500?message=${error.message}`)
        setForm({
          title: data.title,
          banner: data.banner,
          is_active: data.is_active,
          body: data.body,
          description: data.description
        })
        stopLoading()
      })
  }, [])

  const submitForm = async (e: any) => {
    e.preventDefault()
    startLoading()
    try {
      const schema = new NewsFormSchema()
      schema.title = form.title
      schema.body = form.body
      schema.is_active = form.is_active

      const validationErrors = validate(schema)
      if (validationErrors.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
      } else {
        let url = form.banner

        if (!isURL(form.banner)) {
          const [type, _] = form.banner.split(';base64')
          const [__, mimetype] = type.split('data:')
          const [___, ext] = mimetype.split('/')
          await supabase.storage
            .from('company-profile')
            .upload(
              `${form.title}-news-banner.${ext}`,
              decode(form.banner.split('base64,')[1]),
              {
                upsert: true,
                contentType: mimetype
              }
            )
          url = supabase.storage
            .from('company-profile')
            .getPublicUrl(`${form.title}-news-banner.${ext}`, {
              download: true
            }).data.publicUrl
        }
        await supabase
          .from('news')
          .update({
            title: form.title,
            banner: url,
            is_active: form.is_active,
            body: form.body
          })
          .eq('id', id)
      }

      setIsSuccess(true)
      router.push('/master/news')
    } catch (error: any) {
      setIsError(true)
      setErrorObj({
        ...error
      })
    } finally {
      stopLoading()
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />
      <Toast
        show={isSuccess}
        message="News Berhasil Diterbitkan"
        severity="success"
      ></Toast>
      <Toast
        show={isError}
        message="Gagal Menerbitkan News"
        severity="error"
      ></Toast>

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambahkan News Baru" />

          <CardContent>
            <form onSubmit={submitForm}>
              <Grid container padding={6} spacing={6}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    onChange={handleChange('title')}
                    error={Boolean(findErrorProperty('title', errors))}
                    value={form.title}
                    helperText={
                      findErrorProperty('title', errors)
                        ? findErrorProperty('title', errors)?.errors.join(', ')
                        : false
                    }
                    label="Judul News"
                  />
                </Grid>

                <Grid item xs={12}>
                  <label>Status</label>
                </Grid>
                <Grid item xs={12}>
                  <Switch
                    checked={form.is_active}
                    onChange={handleChange('is_active', !form.is_active)}
                  ></Switch>
                </Grid>

                <Grid item xs={12}>
                  <label>Banner News</label>
                </Grid>
                <Grid item xs={12}>
                  {isNotEmpty(form.banner) ? (
                    <img
                      src={form.banner}
                      alt="Client Logo"
                      width={300}
                      height={300}
                    />
                  ) : undefined}
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" component="label">
                    Tambahkan Banner
                    <input
                      type="file"
                      hidden
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        if ((e.target.files as any)[0]) {
                          setForm({
                            ...form,
                            banner: (e.target.files as any)[0]
                          })
                          const reader = new FileReader()
                          const file = (e.target.files as any)[0]
                          reader.onloadend = () => {
                            setForm({ ...form, banner: reader.result as any })
                          }
                          reader.readAsDataURL(file)
                        }
                      }}
                    />
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <label>Isi Konten</label>
                </Grid>
                <Grid item xs={12}>
                  <TextArea
                    handleChange={handleChange('description')}
                    value={form.description}
                    placeholder="Deskripsi Singkat"
                  />
                </Grid>

                <Grid item xs={12}>
                  <label>Isi Konten</label>
                </Grid>
                <Grid item xs={12}>

                  <Editor
                    apiKey='3hjc2rxd3z6x9piofaerc8gzl7p8pwyxw5i1ihdkzrbbm4xq'
                    init={{
                      plugins: 'typography',
                      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link | align lineheight | checklist numlist bullist indent outdent',
                      menubar: false
                    }}
                    initialValue={form.body}
                    onChange={e => {
                      setForm({
                        ...form,
                        body: e.target.getContent()
                      })
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" type="submit" color="primary">
                    Simpan
                  </Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default EditNews
