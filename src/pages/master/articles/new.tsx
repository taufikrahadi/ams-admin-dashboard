import {
  Button,
  Card,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import React, { ChangeEvent, useState } from 'react'
import { useLoading } from 'src/@core/hooks/useLoading'
import Loading from 'src/views/Loading'
import { OutputData } from '@editorjs/editorjs'
import dynamic from 'next/dynamic'
import { IsBoolean, IsNotEmpty, IsString, isNotEmpty } from 'class-validator'
import { supabase } from 'src/configs/supabase'
import { decode } from 'base64-arraybuffer'
import { useRouter } from 'next/router'
import Toast from 'src/views/Toast'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import TextArea from 'src/@core/components/text-area/TextArea'
import { useGuard } from 'src/@core/hooks/useGuard'
import { Editor } from '@tinymce/tinymce-react'

const EditorBlock = dynamic(
  () => import('../../../@core/components/editor/EditorNoSSR'),
  { ssr: false }
)

export class ArticleFormSchema {
  @IsString()
  @IsNotEmpty()
  title: string

  banner: string

  @IsBoolean()
  @IsNotEmpty()
  is_active: boolean

  body: string

  @IsString()
  @IsNotEmpty()
  description: string
}

function NewArticle() {
  const [loading, startLoading, stopLoading] = useLoading(false)

  const [data, setData] = useState<OutputData>()
  const [form, setForm] = useState({
    title: '',
    banner: '',
    is_active: true,
    body: '',
    description: '',
  })
  const [isSuccess, setIsSuccess] = useState(false)
  const [isError, setIsError] = useState(false)
  const handleChange = useHandleChange<ArticleFormSchema>(setForm, form)
  const { errors, validate } = useForm()
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })

  const guard = useGuard()
  const router = useRouter()

  const submitForm = async (e: any) => {
    e.preventDefault()

    try {
      startLoading()

      const schema = new ArticleFormSchema()
      schema.title = form.title
      schema.is_active = form.is_active
      schema.description = form.description

      const validationErrors = validate(schema)
      if (validationErrors.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
      } else {
        const [type, _] = form.banner.split(';base64')
        const [__, mimetype] = type.split('data:')
        const [___, ext] = mimetype.split('/')
        await supabase.storage
          .from('company-profile')
          .upload(
            `${form.title}-articles-banner.${ext}`,
            decode(form.banner.split('base64,')[1]),
            {
              upsert: true,
              contentType: mimetype
            }
          )
        const url = supabase.storage
          .from('company-profile')
          .getPublicUrl(`${form.title}-articles-banner.${ext}`, { download: true })
        await supabase.from('articles').insert({
          title: form.title,
          banner: url.data.publicUrl,
          is_active: form.is_active,
          body: form.body,
          description: form.description
        })

        setIsSuccess(true)
        router.push('/master/articles')
      }
    } catch (error: any) {
      setIsError(true)
      setErrorObj({
        ...error
      })
    } finally {
      stopLoading()
    }
  }

  return (
    <Grid container spacing={6}>
      <Loading show={loading}></Loading>
      <Toast
        show={isSuccess}
        message="Artikel Berhasil Diterbitkan"
        severity="success"
      ></Toast>
      <Toast
        show={isError}
        message="Gagal Menerbitkan Artikel"
        severity="error"
      ></Toast>

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Buat Article" />

          <form onSubmit={submitForm}>
            <Grid container padding={6} spacing={6}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChange('title')}
                  error={Boolean(findErrorProperty('title', errors))}
                  helperText={
                    findErrorProperty('title', errors)
                      ? findErrorProperty('title', errors)?.errors.join(', ')
                      : false
                  }
                  label="Judul Artikel"
                />
              </Grid>

              <Grid item xs={12}>
                <label>Status</label>
              </Grid>
              <Grid item xs={12}>
                <Switch
                  checked={form.is_active}
                  onChange={handleChange('is_active', !form.is_active)}
                ></Switch>
              </Grid>

              <Grid item xs={12}>
                <label>Banner Artikel</label>
              </Grid>
              <Grid item xs={12}>
                {isNotEmpty(form.banner) ? (
                  <img
                    src={form.banner}
                    alt="Client Logo"
                    width={300}
                    height={300}
                  />
                ) : undefined}
              </Grid>
              <Grid item xs={12}>
                <Button variant="contained" component="label">
                  Tambahkan Banner
                  <input
                    type="file"
                    hidden
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                      if ((e.target.files as any)[0]) {
                        setForm({
                          ...form,
                          banner: (e.target.files as any)[0]
                        })
                        const reader = new FileReader()
                        const file = (e.target.files as any)[0]
                        reader.onloadend = () => {
                          setForm({ ...form, banner: reader.result as any })
                        }
                        reader.readAsDataURL(file)
                      }
                    }}
                  />
                </Button>
              </Grid>

              <Grid item xs={12}>
                <TextArea
                  handleChange={handleChange('description')}
                  value={form.description}
                  placeholder="Deskripsi Singkat"
                />
              </Grid>

              <Grid item xs={12}>
                <label>Isi Konten</label>
              </Grid>
              <Grid item xs={12}>

                <Editor
                  apiKey='3hjc2rxd3z6x9piofaerc8gzl7p8pwyxw5i1ihdkzrbbm4xq'
                  init={{
                    plugins: 'typography',
                    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link | align lineheight | checklist numlist bullist indent outdent',
                    menubar: false
                  }}
                  initialValue="Anakmudasukses"
                  onChange={e => {
                    setForm({
                      ...form,
                      body: e.target.getContent()
                    })
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Button variant="contained" type="submit" color="primary">
                  Simpan
                </Button>
              </Grid>
            </Grid>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default NewArticle
