import React, { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { ArticleFormSchema } from '../new'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { useRouter } from 'next/router'
import {
  Button,
  Card,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import Loading from 'src/views/Loading'
import { useLoading } from 'src/@core/hooks/useLoading'
import Toast from 'src/views/Toast'
import { isNotEmpty, isURL } from 'class-validator'
import dynamic from 'next/dynamic'
import { OutputData } from '@editorjs/editorjs'
import { supabase } from 'src/configs/supabase'
import { decode } from 'base64-arraybuffer'
import TextArea from 'src/@core/components/text-area/TextArea'
import { useGuard } from 'src/@core/hooks/useGuard'
import { Editor } from '@tinymce/tinymce-react'

const EditorBlock = dynamic(
  () => import('../../../../@core/components/editor/EditorNoSSR'),
  { ssr: false }
)

function EditArticle() {
  const router = useRouter()
  const { id } = router.query
  const guard = useGuard()
  const fetchArticle = useCallback(() => {
    startLoading()
    return supabase
      .from('articles')
      .select('*')
      .eq('id', id)
      .single()
      .then(({ data, error }) => {
        console.log(data.body)

        setForm({
          title: data.title,
          banner: data.banner,
          is_active: data.is_active,
          body: data.body,
          description: data.description
        })
        stopLoading()

        return data
      })
  }, [])

  const [content, setContent] = useState<OutputData>()
  const [form, setForm] = useState<ArticleFormSchema>({
    title: '',
    banner: '',
    is_active: true,
    body: '',
    description: ''
  })
  const [isSuccess, setIsSuccess] = useState(false)
  const [isError, setIsError] = useState(false)
  const handleChange = useHandleChange<ArticleFormSchema>(setForm, form)
  const { errors, validate } = useForm()
  const [loading, startLoading, stopLoading] = useLoading(true)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })

  const submitForm = async (e: any) => {
    e.preventDefault()
    try {
      console.log(isURL(form.banner))
      const schema = new ArticleFormSchema()
      schema.banner = form.banner
      if (!isURL(form.banner)) {
        const [type, _] = form.banner.split(';base64')
        const [__, mimetype] = type.split('data:')
        const [___, ext] = mimetype.split('/')
        await supabase.storage
          .from('company-profile')
          .upload(
            `${form.title}-articles-banner.${ext}`,
            decode(form.banner.split('base64,')[1]),
            {
              upsert: true,
              contentType: mimetype,
              cacheControl: 'max-age=0'
            }
          ).then(({ data }) => { console.log(data) })

        const url = supabase.storage
          .from('company-profile')
          .getPublicUrl(`${form.title}-articles-banner.${ext}`, { download: true })
        console.log(url.data.publicUrl)

        schema.banner = url.data.publicUrl
      }

      console.log(schema.banner);


      schema.title = form.title
      schema.is_active = form.is_active
      schema.body = form.body

      await supabase.from('articles').update(schema).eq('id', id)
      setIsSuccess(true)
      router.push('/master/articles')
    } catch (error: any) {
      setIsError(true)
      setErrorObj({
        ...error
      })
    }
  }

  useEffect(() => {
    fetchArticle()
  }, [])

  return (
    <Grid container spacing={6}>
      <Loading show={loading}></Loading>
      <Toast
        show={isSuccess}
        message="Artikel Berhasil Diupdate"
        severity="success"
      ></Toast>
      <Toast
        show={isError}
        message="Gagal Menerbitkan Artikel"
        severity="error"
      ></Toast>

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Buat Article" />

          <form onSubmit={submitForm}>
            <Grid container padding={6} spacing={6}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  value={form.title}
                  onChange={handleChange('title')}
                  error={Boolean(findErrorProperty('title', errors))}
                  helperText={
                    findErrorProperty('title', errors)
                      ? findErrorProperty('title', errors)?.errors.join(', ')
                      : false
                  }
                  label="Judul Artikel"
                />
              </Grid>

              <Grid item xs={12}>
                <label>Status</label>
              </Grid>
              <Grid item xs={12}>
                <Switch
                  defaultChecked={form.is_active}
                  checked={form.is_active}
                  onChange={handleChange('is_active', !form.is_active)}
                ></Switch>
              </Grid>

              <Grid item xs={12}>
                <label>Banner Artikel</label>
              </Grid>
              <Grid item xs={12}>
                {isNotEmpty(form.banner) ? (
                  <img
                    src={form.banner}
                    alt="Client Logo"
                    width={300}
                    height={300}
                  />
                ) : undefined}
              </Grid>
              <Grid item xs={12}>
                <Button variant="contained" component="label">
                  Tambahkan Banner
                  <input
                    type="file"
                    hidden
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                      if ((e.target.files as any)[0]) {
                        setForm({
                          ...form,
                          banner: (e.target.files as any)[0]
                        })
                        const reader = new FileReader()
                        const file = (e.target.files as any)[0]
                        reader.onloadend = () => {
                          setForm({ ...form, banner: reader.result as any })
                        }
                        reader.readAsDataURL(file)
                      }
                    }}
                  />
                </Button>
              </Grid>
              <Grid item xs={12}>
                <TextArea
                  handleChange={handleChange('description')}
                  value={form.description}
                  placeholder="Deskripsi Singkat"
                />
              </Grid>

              <Grid item xs={12}>
                <label>Isi Konten</label>
              </Grid>
              <Grid item xs={12}>

                <Editor
                  apiKey='3hjc2rxd3z6x9piofaerc8gzl7p8pwyxw5i1ihdkzrbbm4xq'
                  init={{
                    plugins: 'typography',
                    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link | align lineheight | checklist numlist bullist indent outdent',
                    menubar: false
                  }}
                  initialValue={form.body}
                  onChange={e => {
                    setForm({
                      ...form,
                      body: e.target.getContent()
                    })
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Button variant="contained" type="submit" color="primary">
                  Simpan
                </Button>
              </Grid>
            </Grid>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default EditArticle
