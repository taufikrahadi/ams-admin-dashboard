import { Button, TableCell } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { DeleteOutline, Pencil } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import React, { useCallback, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useLoading } from 'src/@core/hooks/useLoading'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

function Article() {
  const columns: ITableColumn[] = [
    {
      name: 'no',
      title: 'No',
      align: 'left',
      component: ''
    },
    {
      name: 'title',
      title: 'Judul',
      align: 'left',
      component: ''
    },
    {
      name: 'is_active',
      title: 'Status',
      align: 'center',
      component: '',
      formatter(value) {
        return value ? 'AKTIF' : 'TIDAK AKTIF'
      }
    },
    {
      name: 'action',
      title: 'Aksi',
      component: '',
      align: 'center'
    }
  ]

  const router = useRouter()

  const supabase = useSupabaseClient()
  const [articles, setArticles] = useState<any[]>([])

  useGuard()

  const deleteData = (id: string) => {
    return supabase.from('articles').delete().eq('id', id)
  }

  const fetchArticles = useCallback(() => {
    return supabase
      .from('articles')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)

        setArticles(data as any[])
      })
  }, [supabase])

  return (
    <CrudLayout
      columns={columns}
      datas={articles}
      title="List Artikel"
      fetchData={fetchArticles}
      deleteData={deleteData}
      actionChildren={(row, setOpenDialog, selectedId) => {
        return (
          <TableCell align="center">
            <Button
              style={{ marginRight: '1rem' }}
              color="warning"
              variant="outlined"
              onClick={() => router.push(`/master/articles/edit/${row.id}`)}
            >
              <Pencil />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                selectedId(row.id)
                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default Article
