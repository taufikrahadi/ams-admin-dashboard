import { Button, TableCell } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { PostgrestError } from '@supabase/supabase-js'
import { DeleteOutline, Pencil } from 'mdi-material-ui'
import router, { useRouter } from 'next/router'
import React, { useCallback, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useLoading } from 'src/@core/hooks/useLoading'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

const columns: ITableColumn[] = [
  {
    name: 'no',
    title: 'No',
    align: 'left',
    component: ''
  },
  {
    name: 'name',
    title: 'Nama Klien',
    align: 'left',
    component: ''
  },
  {
    name: 'is_active',
    title: 'Status',
    component: '',
    align: 'left',
    formatter(value) {
      return value ? 'AKTIF' : 'TIDAK AKTIF'
    }
  },
  {
    name: 'action',
    title: 'Aksi',
    component: '',
    align: 'center'
  }
]

function Client() {
  const supabase = useSupabaseClient()

  const [clients, setClients] = useState([])
  const router = useRouter()

  const guard = useGuard()

  const fetchData = useCallback(() => {
    return supabase
      .from('clients')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)

        setClients(data as any)
      })
  }, [])

  const deleteData = (id: any) => {
    return supabase.from('clients').delete().eq('id', id)
  }

  return (
    <CrudLayout
      columns={columns}
      datas={clients}
      title="List Klien"
      fetchData={fetchData}
      deleteData={deleteData}
      actionChildren={(row, setOpenDialog, setSelectedId) => {
        return (
          <TableCell align="center">
            <Button
              style={{ marginRight: '1rem' }}
              color="warning"
              variant="outlined"
              onClick={() => router.push(`/master/client/edit/${row.id}`)}
            >
              <Pencil />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                setSelectedId(row.id)
                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default Client
