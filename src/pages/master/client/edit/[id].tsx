import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useRouter } from 'next/router'
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { useLoading } from 'src/@core/hooks/useLoading'
import { ClientFormSchema } from '../new'
import { PostgrestError } from '@supabase/supabase-js'
import {
  Button,
  Card,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { decode } from 'base64-arraybuffer'
import { isURL } from 'class-validator'
import { useGuard } from 'src/@core/hooks/useGuard'

function EditClient() {
  const router = useRouter()
  const { id } = router.query
  const supabase = useSupabaseClient()
  const [loading, startLoading, stopLoading] = useLoading(true)
  const [errorObj, setErrorObj] = useState({
    code: '',
    details: '',
    hint: '',
    message: ''
  } as PostgrestError)
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const guard = useGuard()

  const [form, setForm] = useState<ClientFormSchema>({
    name: '',
    logo: '',
    is_active: true
  })

  const handleChange = useHandleChange<ClientFormSchema>(setForm, form)
  const { errors, validate } = useForm()

  const fetchData = useCallback(() => {
    return supabase
      .from('clients')
      .select('*')
      .eq('id', id)
      .single()
      .then(({ data, error }) => {
        if (error) {
          setIsError(true)
          setErrorObj({
            ...error
          })
        }

        setForm({
          ...(data as any)
        })

        stopLoading()
      })
  }, [])

  const submitUpdate = async (e: any) => {
    e.preventDefault()
    startLoading()
    const schema = new ClientFormSchema()
    schema.name = form.name
    schema.is_active = form.is_active

    const validationErrors = validate(schema)
    if (validationErrors.length) {
      setIsError(true)
      setErrorObj({
        ...errorObj,
        message: 'Please check your data.'
      } as PostgrestError)
      stopLoading()
    } else {
      let expectedLogo: string = form.logo
      if (!isURL(form.logo)) {
        const image = form.logo.split('base64,')[1]

        await supabase.storage
          .from('company-profile')
          .upload(`${form.name}-client-logo.png`, decode(image), {
            contentType: 'image/png',
            upsert: true,
            cacheControl: 'max-age=0'
          })

        const {
          data: { publicUrl }
        } = supabase.storage
          .from('company-profile')
          .getPublicUrl(`${form.name}-client-logo.png`, { download: true })

        expectedLogo = publicUrl
      }
      supabase
        .from('clients')
        .update({
          name: form.name,
          is_active: form.is_active,
          logo: expectedLogo
        })
        .eq('id', id)
        .then(() => {
          stopLoading()
          router.push('/master/client')
        })
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast message={errorObj.message} severity="error" show={isError} />

      <Toast
        message="Data berhasil diubah."
        severity="success"
        show={isSuccess}
      />

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Ubah Data Client" />

          <form onSubmit={submitUpdate}>
            <Grid container padding={6} spacing={6}>
              <Grid item xs={12}>
                <TextField
                  label="Nama Klien"
                  fullWidth
                  variant="outlined"
                  value={form.name}
                  onChange={handleChange('name')}
                  error={Boolean(findErrorProperty('name', errors))}
                  helperText={
                    findErrorProperty('name', errors)
                      ? findErrorProperty('name', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <label>Logo</label>
              </Grid>

              <Grid item xs={12}>
                <Grid item xs={12}>
                  <img
                    src={form.logo}
                    alt="Client Logo"
                    width={300}
                    height={300}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Button variant="contained" component="label">
                    Tambahkan Logo
                    <input
                      type="file"
                      hidden
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        if ((e.target.files as any)[0]) {
                          setForm({
                            ...form,
                            logo: (e.target.files as any)[0]
                          })
                          const reader = new FileReader()
                          const file = (e.target.files as any)[0]
                          reader.onloadend = () => {
                            setForm({ ...form, logo: reader.result as any })
                          }
                          reader.readAsDataURL(file)
                        }
                      }}
                    />
                  </Button>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid xs={4}>
                  <label>Status</label>
                </Grid>
                <Grid>
                  <Switch
                    checked={form.is_active}
                    onChange={handleChange('is_active', !form.is_active)}
                  ></Switch>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Button type="submit" variant="contained">
                  Simpan Data
                </Button>
              </Grid>
            </Grid>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default EditClient
