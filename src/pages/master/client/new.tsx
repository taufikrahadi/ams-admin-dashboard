import {
  Button,
  Card,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { decode } from 'base64-arraybuffer'
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
  isNotEmpty
} from 'class-validator'
import router from 'next/router'
import React, { ChangeEvent, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export class ClientFormSchema {
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  name: string

  @IsOptional()
  logo: string

  @IsBoolean()
  @IsNotEmpty()
  is_active: boolean
}

function NewClient() {
  const [form, setForm] = useState({
    name: '',
    logo: '',
    is_active: true
  })

  const guard = useGuard()
  const [loading, startLoading, stopLoading] = useLoading(false)
  const handleChange = useHandleChange<ClientFormSchema>(setForm, form)
  const { errors, validate } = useForm()
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })

  const submitForm = async (e: any) => {
    e.preventDefault()
    try {
      startLoading()
      const schema = new ClientFormSchema()
      schema.name = form.name
      schema.is_active = form.is_active

      const validationErrors = validate(schema)
      if (validationErrors.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
        stopLoading()
      } else {
        const image = form.logo.split('base64,')[1]

        await supabase.storage
          .from('company-profile')
          .upload(`${form.name}-client-logo.png`, decode(image), {
            upsert: true,
            contentType: 'image/png',
            cacheControl: 'max-age=1'
          })

        const url = await supabase.storage
          .from('company-profile')
          .getPublicUrl(`${form.name}-client-logo.png`, {})

        await supabase
          .from('clients')
          .insert({ ...form, logo: url.data.publicUrl })

        stopLoading()
        setIsSuccess(true)
        router.push('/master/client')
      }
    } catch (error: any) {
      setIsError(true)
      setErrorObj({
        ...error
      })
    }
  }

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast show={isError} message={errorObj.message} severity="error" />
      <Toast
        show={isSuccess}
        message="Data berhasil ditambahkan."
        severity="success"
      />

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambah Data" />

          <form onSubmit={submitForm}>
            <Grid container padding={6} spacing={6}>
              <Grid item xs={12}>
                <TextField
                  label="Nama Klien"
                  fullWidth
                  variant="outlined"
                  value={form.name}
                  onChange={handleChange('name')}
                  error={Boolean(findErrorProperty('name', errors))}
                  helperText={
                    findErrorProperty('name', errors)
                      ? findErrorProperty('name', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <label>Logo</label>
              </Grid>

              <Grid item xs={12}>
                <Grid item xs={12}>
                  {isNotEmpty(form.logo) ? (
                    <img
                      src={form.logo}
                      alt="Client Logo"
                      width={300}
                      height={300}
                    />
                  ) : undefined}
                </Grid>

                <Grid item xs={12}>
                  <Button variant="contained" component="label">
                    Tambahkan Logo
                    <input
                      type="file"
                      hidden
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        if ((e.target.files as any)[0]) {
                          setForm({
                            ...form,
                            logo: (e.target.files as any)[0]
                          })
                          const reader = new FileReader()
                          const file = (e.target.files as any)[0]
                          reader.onloadend = () => {
                            setForm({ ...form, logo: reader.result as any })
                          }
                          reader.readAsDataURL(file)
                        }
                      }}
                    />
                  </Button>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid xs={4}>
                  <label>Status</label>
                </Grid>
                <Grid>
                  <Switch
                    checked={form.is_active}
                    onChange={handleChange('is_active', !form.is_active)}
                  ></Switch>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Button type="submit" variant="contained">
                  Simpan Data
                </Button>
              </Grid>
            </Grid>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default NewClient
