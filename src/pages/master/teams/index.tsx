import { Button, TableCell } from '@mui/material'
import { DeleteOutline, Pencil } from 'mdi-material-ui'
import router from 'next/router'
import React, { useCallback, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { supabase } from 'src/configs/supabase'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

const columns: ITableColumn[] = [
  {
    name: 'no',
    title: 'No',
    component: '',
    align: 'center'
  },
  {
    name: 'fullname',
    title: 'Nama',
    align: 'left',
    component: ''
  },
  {
    name: 'role',
    title: 'Jabatan',
    align: 'left',
    component: ''
  },
  {
    name: 'is_active',
    title: 'Status',
    align: 'center',
    component: '',
    formatter(value) {
      return value ? 'AKTIF' : 'TIDAK AKTIF'
    },
  }
]

function Teams() {
  const [teams, setTeams] = useState<any[]>([])

  const fetchData = useCallback(() => {
    return supabase
      .from('teams')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)
        setTeams(data as any)
      })
  }, [supabase])
  const deletedData = useCallback((selectedId: any) => {
    return supabase.from('teams').delete().eq('id', selectedId)
  }, [supabase])

  useGuard()
  return (
    <CrudLayout
      columns={columns}
      fetchData={fetchData}
      datas={teams}
      deleteData={deletedData}
      title="Teams"
      actionChildren={(row, setOpenDialog, setSelectedId) => {
        return (
          <TableCell align="center">
            <Button
              style={{ marginRight: '1rem' }}
              color="warning"
              variant="outlined"
              onClick={() => router.push(`/master/teams/edit/${row.id}`)}
            >
              <Pencil />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                setSelectedId(row.id)
                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default Teams
