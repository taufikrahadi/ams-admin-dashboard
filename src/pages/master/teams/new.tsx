import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { decode } from 'base64-arraybuffer'
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import router from 'next/router'
import React, { ChangeEvent, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export class TeamFormSchema {
  @IsString()
  @IsNotEmpty()
  fullname: string

  @IsString()
  @IsNotEmpty()
  role: string

  photo: string

  @IsString()
  @IsOptional()
  description: string

  @IsOptional()
  instagram: string

  @IsOptional()
  facebook: string

  @IsOptional()
  twitter: string

  @IsOptional()
  linkedin: string

  @IsBoolean()
  is_active: boolean
}

function NewTeam() {
  const [loading, startLoading, stopLoading] = useLoading(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [isError, setIsError] = useState(false)
  const { errors, validate } = useForm()
  const [form, setForm] = useState<TeamFormSchema>({
    fullname: '',
    description: '',
    facebook: '',
    instagram: '',
    is_active: true,
    linkedin: '',
    photo: '',
    role: '',
    twitter: ''
  })
  const handleChange = useHandleChange<TeamFormSchema>(setForm, form)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })

  const submitForm = async (e: any) => {
    e.preventDefault()
    startLoading()

    try {
      const schema = new TeamFormSchema()
      Object.assign(schema, form)

      const validationErrors = validate(schema)
      if (validationErrors.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
      } else {
        const [type, _] = form.photo.split(';base64')
        const [__, mimetype] = type.split('data:')
        const [___, ext] = mimetype.split('/')
        const filePath = `${form.fullname.split(' ')[0]}-profile-photo.${ext}`
        await supabase.storage
          .from('company-profile')
          .upload(filePath, decode(form.photo.split('base64,')[1]), {
            contentType: mimetype
          })

        const {
          data: { publicUrl }
        } = supabase.storage
          .from('company-profile')
          .getPublicUrl(filePath, { download: true })

        await supabase
          .from('teams')
          .insert({ ...schema, photo: publicUrl })
          .select()

        setIsSuccess(true)
        router.push('/master/teams')
      }
    } catch (error: any) {
      setIsError(true)
      setErrorObj({
        ...error
      })
    } finally {
      stopLoading()
    }
  }

  useGuard()

  return (
    <Grid container>
      <Loading show={loading} />
      <Toast
        show={isSuccess}
        message="Data berhasil ditambahkan"
        severity="success"
      />
      <Toast show={isError} message={errorObj.message} severity="error" />

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambah Data" />

          <form onSubmit={submitForm}>
            <CardContent>
              <Grid container spacing={6}>
                <Grid item xs={12}>
                  <TextField
                    label="Nama Lengkap"
                    value={form.fullname}
                    fullWidth
                    onChange={handleChange('fullname')}
                    error={Boolean(findErrorProperty('fullname', errors))}
                    helperText={
                      findErrorProperty('fullname', errors)
                        ? findErrorProperty('fullname', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    label="Jabatan"
                    value={form.role}
                    fullWidth
                    onChange={handleChange('role')}
                    error={Boolean(findErrorProperty('role', errors))}
                    helperText={
                      findErrorProperty('role', errors)
                        ? findErrorProperty('role', errors)?.errors.join(', ')
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12}>
                  <label>Foto</label>
                </Grid>

                <Grid item xs={12}>
                  <Button component="label" color="primary" variant="contained">
                    <input
                      type="file"
                      hidden
                      multiple
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        if ((e.target.files as any)[0]) {
                          setForm({
                            ...form,
                            photo: (e.target.files as any)[0]
                          })
                          const reader = new FileReader()
                          const file = (e.target.files as any)[0]
                          reader.onloadend = () => {
                            setForm({ ...form, photo: reader.result as any })
                          }
                          reader.readAsDataURL(file)
                        }
                      }}
                    />
                    Tambahkan Foto
                  </Button>
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    label="Facebook"
                    fullWidth
                    onChange={handleChange('facebook')}
                    placeholder="https://facebook.com/ariamin"
                    variant="outlined"
                    value={form.facebook}
                    error={Boolean(findErrorProperty('facebook', errors))}
                    helperText={
                      findErrorProperty('facebook', errors)
                        ? findErrorProperty('facebook', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    label="Instagram"
                    fullWidth
                    onChange={handleChange('instagram')}
                    variant="outlined"
                    placeholder="https://instagram.com/ariamin"
                    value={form.instagram}
                    error={Boolean(findErrorProperty('instagram', errors))}
                    helperText={
                      findErrorProperty('instagram', errors)
                        ? findErrorProperty('instagram', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    label="Linkedin"
                    fullWidth
                    onChange={handleChange('linkedin')}
                    variant="outlined"
                    placeholder="https://linkedin.com/in/nama-akun"
                    value={form.linkedin}
                    error={Boolean(findErrorProperty('linkedin', errors))}
                    helperText={
                      findErrorProperty('linkedin', errors)
                        ? findErrorProperty('', errors)?.errors.join(', ')
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    label="Twitter"
                    fullWidth
                    onChange={handleChange('twitter')}
                    variant="outlined"
                    placeholder="https://twitter.com/RahadiTaufik"
                    value={form.twitter}
                    error={Boolean(findErrorProperty('twitter', errors))}
                    helperText={
                      findErrorProperty('twitter', errors)
                        ? findErrorProperty('twitter', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12}>
                  <label>Status</label>
                </Grid>
                <Grid item xs={12}>
                  <Switch
                    checked={form.is_active}
                    onChange={handleChange('is_active', !form.is_active)}
                  ></Switch>
                </Grid>

                <Grid item xs={12}>
                  <Button type="submit" variant="contained" color="primary">
                    Simpan Data
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default NewTeam
