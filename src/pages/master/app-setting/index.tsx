import {
  Card,
  CardHeader,
  Grid,
  TextField,
  TextareaAutosize as BaseTextArea,
  styled,
  Button
} from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { decode } from 'base64-arraybuffer'
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
  isURL
} from 'class-validator'
import { ApplicationSettingsOutline } from 'mdi-material-ui'
import React, {
  ChangeEvent,
  FormEventHandler,
  useCallback,
  useEffect,
  useState
} from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { useSupabaseAdmin } from 'src/@core/hooks/useSupabaseAdmin'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export class AppSettingSchema {
  @IsString()
  @IsNotEmpty()
  title: string

  @IsString()
  @IsNotEmpty()
  description: string

  @IsString()
  @IsNotEmpty()
  whatsapp: string

  @IsUrl()
  @IsOptional()
  facebook: string

  @IsUrl()
  @IsOptional()
  instagram: string

  @IsUrl()
  @IsOptional()
  tiktok: string

  @IsUrl()
  @IsOptional()
  youtube: string

  @IsEmail()
  @IsNotEmpty()
  email: string

  @IsOptional()
  logo: string

  @IsString()
  @IsNotEmpty()
  address: string
}

const blue = {
  100: '#DAECFF',
  200: '#b6daff',
  400: '#3399FF',
  500: '#007FFF',
  600: '#0072E5',
  900: '#003A75'
}

const grey = {
  50: '#f6f8fa',
  100: '#eaeef2',
  200: '#d0d7de',
  300: '#afb8c1',
  400: '#8c959f',
  500: '#6e7781',
  600: '#57606a',
  700: '#424a53',
  800: '#32383f',
  900: '#24292f'
}

const TextareaAutosize = styled(BaseTextArea)(
  ({ theme }) => `
  width: 100%;
  font-family: IBM Plex Sans, sans-serif;
  font-size: 0.875rem;
  font-weight: 400;
  line-height: 1.5;
  padding: 12px;
  border-radius: 12px 12px 0 12px;
  color: ${theme.palette.mode === 'dark' ? grey[300] : grey[900]};
  background: ${theme.palette.mode === 'dark' ? grey[900] : '#fff'};
  border: 1px solid ${theme.palette.mode === 'dark' ? grey[700] : grey[200]};
  box-shadow: 0px 2px 24px ${theme.palette.mode === 'dark' ? blue[900] : blue[100]
    };

  &:hover {
    border-color: ${blue[400]};
  }

  &:focus {
    border-color: ${blue[400]};
    box-shadow: 0 0 0 3px ${theme.palette.mode === 'dark' ? blue[600] : blue[200]
    };
  }

  // firefox
  &:focus-visible {
    outline: 0;
  }
`
)

function Index() {
  const supabase = useSupabaseClient()
  const [appSetting, setAppSetting] = useState({
    id: '',
    title: '',
    description: '',
    whatsapp: '',
    facebook: '',
    instagram: '',
    tiktok: '',
    youtube: '',
    email: '',
    logo: '',
    address: ''
  })
  const [loading, startLoading, stopLoading] = useLoading(false)
  const { errors, validate } = useForm()
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })
  const handleChange = useHandleChange(setAppSetting, appSetting)
  const guard = useGuard()

  const uploadLogo = async () => {
    if (!isURL(appSetting.logo)) {
      const [type, _] = appSetting.logo.split(';base64')
      const [__, mimetype] = type.split('data:')
      const [___, ext] = mimetype.split('/')
      await supabase.storage
        .from('company-profile')
        .upload(
          `app-logo.${ext}`,
          decode(appSetting.logo.split('base64,')[1]),
          {
            upsert: true,
            contentType: mimetype
          }
        )
      const url = supabase.storage
        .from('company-profile')
        .getPublicUrl(`app-logo.${ext}`, { download: true })

      return url.data.publicUrl
    }

    return appSetting.logo
  }

  const fetchData = useCallback(() => {
    return supabase
      .from('app_settings')
      .select('*')
      .single()
      .then(({ data, error }) => {
        console.log(data)

        if (error) {
          setIsError(true)
          setErrorObj({
            ...errorObj,
            message: 'Internal Server Error'
          } as PostgrestError)
        }

        setAppSetting({ ...data })
        setLogo(data.logo)
      })
  }, [supabase])

  const updateData = async (event: any) => {
    event.preventDefault()
    startLoading()

    try {
      await uploadLogo()
      const url = await supabase.storage
        .from('company-profile')
        .getPublicUrl('logo.png')

      await supabase
        .from('app_settings')
        .update({
          ...appSetting,
          logo: url.data.publicUrl
        })
        .eq('id', appSetting.id)

      await fetchData()
      setIsSuccess(true)
    } catch (error) {
      setIsError(true)
      setErrorObj({
        ...errorObj,
        message: error
      } as PostgrestError)
    } finally {
      stopLoading()
    }
  }

  const [logo, setLogo] = useState('')

  useEffect(() => {
    startLoading()
    fetchData().then(() => {
      stopLoading()
    })
  }, [])

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast
        severity="error"
        show={isError}
        message="Gagal Mengubah Data Setting"
      />

      <Toast
        severity="success"
        show={isSuccess}
        message="Sukses Mengubah Data Setting"
      />

      <Grid item xs={12}>
        <Card>
          <CardHeader title={'Setting Aplikasi'} />

          <form onSubmit={updateData}>
            <Grid container padding={6} spacing={6}>
              <Grid item xs={12}>
                <TextField
                  label="Nama Aplikasi"
                  fullWidth
                  onChange={handleChange('title')}
                  variant="outlined"
                  value={appSetting.title}
                  error={Boolean(findErrorProperty('title', errors))}
                  helperText={
                    findErrorProperty('title', errors)
                      ? findErrorProperty('title', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <label>Deskripsi</label>
              </Grid>
              <Grid item xs={12}>
                <TextareaAutosize
                  minRows={4}
                  placeholder="Deskripsi Aplikasi"
                  value={appSetting.description}
                  onChange={handleChange('description')}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="Whatsapp"
                  fullWidth
                  onChange={handleChange('whatsapp')}
                  variant="outlined"
                  value={appSetting.whatsapp}
                  error={Boolean(findErrorProperty('whatsapp', errors))}
                  helperText={
                    findErrorProperty('whatsapp', errors)
                      ? findErrorProperty('whatsapp', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="facebook"
                  fullWidth
                  onChange={handleChange('facebook')}
                  variant="outlined"
                  value={appSetting.facebook}
                  error={Boolean(findErrorProperty('facebook', errors))}
                  helperText={
                    findErrorProperty('facebook', errors)
                      ? findErrorProperty('facebook', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="instagram"
                  fullWidth
                  onChange={handleChange('instagram')}
                  variant="outlined"
                  value={appSetting.instagram}
                  error={Boolean(findErrorProperty('instagram', errors))}
                  helperText={
                    findErrorProperty('instagram', errors)
                      ? findErrorProperty('instagram', errors)?.errors.join(
                        ', '
                      )
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="tiktok"
                  fullWidth
                  onChange={handleChange('tiktok')}
                  variant="outlined"
                  value={appSetting.tiktok}
                  error={Boolean(findErrorProperty('tiktok', errors))}
                  helperText={
                    findErrorProperty('tiktok', errors)
                      ? findErrorProperty('tiktok', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="youtube"
                  fullWidth
                  onChange={handleChange('youtube')}
                  variant="outlined"
                  value={appSetting.youtube}
                  error={Boolean(findErrorProperty('youtube', errors))}
                  helperText={
                    findErrorProperty('youtube', errors)
                      ? findErrorProperty('youtube', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="email"
                  fullWidth
                  onChange={handleChange('email')}
                  variant="outlined"
                  value={appSetting.email}
                  error={Boolean(findErrorProperty('email', errors))}
                  type="email"
                  helperText={
                    findErrorProperty('email', errors)
                      ? findErrorProperty('email', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <label>Logo</label>
              </Grid>

              <Grid item xs={12}>
                <Grid item xs={12}>
                  <img src={logo} alt="AMS Logo" width={300} height={300} />
                </Grid>

                <Grid item xs={12}>
                  <Button variant="contained" component="label">
                    Ubah Logo
                    <input
                      type="file"
                      hidden
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        if ((e.target.files as any)[0]) {
                          setAppSetting({
                            ...appSetting,
                            logo: (e.target.files as any)[0]
                          })
                          const reader = new FileReader()
                          const file = (e.target.files as any)[0]
                          reader.onloadend = () => {
                            setLogo(reader.result as any)
                          }
                          reader.readAsDataURL(file)
                        }
                      }}
                    />
                  </Button>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <label>Alamat Kantor</label>
              </Grid>

              <Grid item xs={12}>
                <TextareaAutosize
                  minRows={4}
                  placeholder="Alamat Kantor"
                  value={appSetting.address}
                  onChange={handleChange('address')}
                />
              </Grid>

              <Grid item xs={12} alignItems={'end'}>
                <Button type="submit" variant="contained">
                  Simpan Data
                </Button>
              </Grid>
            </Grid>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Index
