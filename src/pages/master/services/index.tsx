import { Button, TableCell } from '@mui/material'
import { Delete, DeleteOutline, Pencil } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import React, { useCallback, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { supabase } from 'src/configs/supabase'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

function Services() {
  const columns: ITableColumn[] = [
    { name: 'no', title: 'No', align: 'center', component: '' },
    { name: 'name', title: 'Nama', align: 'left', component: '' },
    {
      name: 'is_active',
      title: 'Status',
      align: 'left',
      component: '',
      formatter(value) {
        return value ? 'AKTIF' : 'TIDAK AKTIF'
      }
    },
    { name: 'action', title: 'Aksi', align: 'center', component: '' }
  ]

  useGuard()

  const [services, setServices] = useState<any[]>([])
  const router = useRouter()
  const fetchData = useCallback(() => {
    return supabase
      .from('services')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)
        setServices(data as any[])
      })
  }, [supabase])

  const deleteData = useCallback((id: any) => {
    return supabase.from('services').delete().eq('id', id)
  }, [supabase])

  return (
    <CrudLayout
      columns={columns}
      datas={services}
      title="Services"
      fetchData={fetchData}
      deleteData={deleteData}
      actionChildren={(row, setOpenDialog, setSelectedId) => (
        <TableCell>
          <Button
            style={{ marginRight: '1rem' }}
            color="warning"
            variant="outlined"
            onClick={() => router.push(`/master/services/edit/${row.id}`)}
          >
            <Pencil />
          </Button>
          <Button
            color="error"
            variant="outlined"
            onClick={() => {
              setSelectedId(row.id)

              setOpenDialog(true)
            }}
          >
            <DeleteOutline />
          </Button>
        </TableCell>
      )}
    ></CrudLayout>
  )
}

export default Services
