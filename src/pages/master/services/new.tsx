import {
  Button,
  Card,
  CardHeader,
  Grid,
  TextField,
  TextareaAutosize as BaseTextArea,
  styled,
  Switch
} from '@mui/material'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { decode } from 'base64-arraybuffer'
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  isNotEmpty
} from 'class-validator'
import { useRouter } from 'next/router'
import React, { ChangeEvent, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export class ServiceFormSchema {
  @IsString()
  @IsNotEmpty()
  name: string

  @IsBoolean()
  is_active: boolean

  @IsString()
  @IsNotEmpty()
  icon: string

  @IsString()
  @IsOptional()
  description: string

  @IsString()
  @IsNotEmpty()
  banner: string
}

const blue = {
  100: '#DAECFF',
  200: '#b6daff',
  400: '#3399FF',
  500: '#007FFF',
  600: '#0072E5',
  900: '#003A75'
}

const grey = {
  50: '#f6f8fa',
  100: '#eaeef2',
  200: '#d0d7de',
  300: '#afb8c1',
  400: '#8c959f',
  500: '#6e7781',
  600: '#57606a',
  700: '#424a53',
  800: '#32383f',
  900: '#24292f'
}

const TextareaAutosize = styled(BaseTextArea)(
  ({ theme }) => `
  width: 100%;
  font-family: IBM Plex Sans, sans-serif;
  font-size: 0.875rem;
  font-weight: 400;
  line-height: 1.5;
  padding: 12px;
  border-radius: 12px 12px 0 12px;
  color: ${theme.palette.mode === 'dark' ? grey[300] : grey[900]};
  background: ${theme.palette.mode === 'dark' ? grey[900] : '#fff'};
  border: 1px solid ${theme.palette.mode === 'dark' ? grey[700] : grey[200]};
  box-shadow: 0px 2px 24px ${theme.palette.mode === 'dark' ? blue[900] : blue[100]
    };

  &:hover {
    border-color: ${blue[400]};
  }

  &:focus {
    border-color: ${blue[400]};
    box-shadow: 0 0 0 3px ${theme.palette.mode === 'dark' ? blue[600] : blue[200]
    };
  }

  // firefox
  &:focus-visible {
    outline: 0;
  }
`
)

function NewService() {
  const [form, setForm] = useState<ServiceFormSchema>({
    name: '',
    banner: '',
    description: '',
    icon: '',
    is_active: true
  })

  const [loading, startLoading, stopLoading] = useLoading(false)
  const handleChange = useHandleChange<ServiceFormSchema>(setForm, form)
  const { errors, validate } = useForm()
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })
  useGuard()

  const [banner, setBanner] = useState(null)
  const [icon, setIcon] = useState(null)

  const router = useRouter()

  const processIcon = async (name: string, imgVal: any) => {
    const [type, _] = imgVal.split(';base64')
    const [__, mimetype] = type.split('data:')
    const [___, ext] = mimetype.split('/')
    await supabase.storage
      .from('company-profile')
      .upload(`${name}.${ext}`, decode((imgVal as string).split('base64,')[1]), {
        contentType: mimetype,
        upsert: true
      })

    const url = supabase.storage
      .from('company-profile')
      .getPublicUrl(`${name}.${ext}`, { download: true })

    return url.data.publicUrl
  }

  const submitForm = async (e: any) => {
    e.preventDefault()
    startLoading()
    try {
      const [iconName, bannerName] = [
        `${Date.now()}-${form.name}-icon`,
        `${Date.now()}-${form.name}-banner`
      ]
      const [iconUrl, bannerUrl] = await Promise.all([
        processIcon(iconName, icon), processIcon(bannerName, banner)
      ])

      const schema = new ServiceFormSchema()
      schema.name = form.name
      schema.description = form.description
      schema.is_active = form.is_active
      schema.icon = iconUrl
      schema.banner = bannerUrl

      const validationError = validate(schema)
      if (validationError.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
        stopLoading()
      } else {
        await supabase.from('services').insert(schema)
        stopLoading()
        setIsSuccess(true)
        router.push('/master/services')
      }
    } catch (error) {
      console.log(error);

      setIsError(true)
      setErrorObj({
        ...(error as any)
      })
    } finally {
      stopLoading()
    }
  }

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast show={isError} message={errorObj.message} severity="error" />
      <Toast
        show={isSuccess}
        message="Data berhasil ditambahkan."
        severity="success"
      />

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambah Data" />

          <form onSubmit={submitForm}>
            <Grid container padding={6} spacing={6}>
              <Grid item xs={12}>
                <TextField
                  label="Nama Service"
                  fullWidth
                  variant="outlined"
                  value={form.name}
                  onChange={handleChange('name')}
                  error={Boolean(findErrorProperty('name', errors))}
                  helperText={
                    findErrorProperty('name', errors)
                      ? findErrorProperty('name', errors)
                      : false
                  }
                />
              </Grid>

              <Grid item xs={12}>
                <TextareaAutosize
                  minRows={4}
                  placeholder="Deskripsi"
                  onChange={handleChange('description')}
                />
              </Grid>

              <Grid item xs={12}>
                <label>Icon</label>
              </Grid>

              <Grid item xs={12}>
                {isNotEmpty(icon) ? (
                  <img
                    src={icon as any}
                    alt="service icon"
                    width={300}
                    height={300}
                  />
                ) : undefined}
              </Grid>

              <Grid item xs={12}>
                <Button variant="contained" component="label" color="primary">
                  <input
                    type="file"
                    accept="image/png"
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                      if ((e.target.files as any)[0]) {
                        const reader = new FileReader()
                        const file = (e.target.files as any)[0]
                        reader.onloadend = () => {
                          setIcon(reader.result as any)
                        }
                        reader.readAsDataURL(file)
                      }
                    }}
                  ></input>
                </Button>
              </Grid>

              <Grid item xs={12}>
                <label>Banner</label>
              </Grid>

              <Grid item xs={12}>
                {isNotEmpty(banner) ? (
                  <img
                    src={banner as any}
                    alt="service banner"
                    width={300}
                    height={300}
                  />
                ) : undefined}
              </Grid>

              <Grid item xs={12}>
                <Button variant="contained" component="label" color="primary">
                  <input
                    type="file"
                    accept="image/jpg, image/jpeg"
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                      if ((e.target.files as any)[0]) {
                        const reader = new FileReader()
                        const file = (e.target.files as any)[0]
                        reader.onloadend = () => {
                          setBanner(reader.result as any)
                        }
                        reader.readAsDataURL(file)
                      }
                    }}
                  />
                </Button>
              </Grid>

              <Grid item xs={12}>
                <label>Status</label>
              </Grid>

              <Grid item xs={12}>
                <Switch
                  checked={form.is_active}
                  onChange={handleChange('is_active')}
                ></Switch>
              </Grid>

              <Grid item xs={12} alignContent="end">
                <Button type="submit" variant="contained" color="primary">
                  Simpan
                </Button>
              </Grid>
            </Grid>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default NewService
