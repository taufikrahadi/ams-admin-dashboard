import { useRouter } from 'next/router'
import React, { useCallback, useEffect, useState } from 'react'
import { useLoading } from 'src/@core/hooks/useLoading'
import { ProjectCategoryFormSchema } from '../new'
import { useForm } from 'src/configs/validateSync'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { Card, CardHeader, Grid } from '@mui/material'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'
import ProjectCategory from 'src/views/forms/ProjectCategory'
import { useGuard } from 'src/@core/hooks/useGuard'

function Edit() {
  const router = useRouter()
  const { id } = router.query
  const [loading, startLoading, stopLoading] = useLoading(true)

  const [form, setForm] = useState<ProjectCategoryFormSchema>({
    name: '',
    is_active: true
  })
  const { errors, validate } = useForm()
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    hint: '',
    details: '',
    message: ''
  })
  const handleChange = useHandleChange(setForm, form)
  const supabase = useSupabaseClient()
  useGuard()

  const fetchData = useCallback(() => {
    startLoading()
    return supabase
      .from('project_categories')
      .select('*')
      .eq('id', id)
      .single()
      .then(({ data, error }) => {
        stopLoading()
        if (error) {
          setIsError(true)
          setErrorObj(error)
        } else {
          setForm({
            name: data.name,
            is_active: data.is_active
          })
        }
      })
  }, [supabase, id])

  useEffect(() => {
    fetchData()
  }, [fetchData, id])

  const handleSubmit = (e: any) => {
    e.preventDefault()
    startLoading()

    const schema = new ProjectCategoryFormSchema()
    schema.name = form.name
    schema.is_active = form.is_active
    const validationError = validate(schema)
    if (!validationError.length) {
      return supabase
        .from('project_categories')
        .update({
          ...schema
        })
        .eq('id', id)
        .then(({ error }) => {
          stopLoading()
          if (error) {
            setIsError(true)
            setErrorObj(error)
          } else {
            setIsSuccess(true)
            router.push('/master/project')
          }
        })
    } else {
      stopLoading()
      setErrorObj({
        ...errorObj,
        message: 'Please check your data'
      } as PostgrestError)
    }
  }

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast severity="error" show={isError} message={errorObj.message} />

      <Toast
        severity="success"
        show={isSuccess}
        message="Data berhasil ditambahkan."
      />

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambah Data" />

          <form onSubmit={handleSubmit}>
            <ProjectCategory
              form={form}
              handleChange={handleChange}
              errors={errors}
            />
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Edit
