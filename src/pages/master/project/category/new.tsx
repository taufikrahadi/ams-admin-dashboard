import { Grid, Card, CardHeader } from '@mui/material'
import { useSupabaseClient, useUser } from '@supabase/auth-helpers-react'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'
import ProjectCategory from 'src/views/forms/ProjectCategory'

export class ProjectCategoryFormSchema {
  @IsNotEmpty()
  @IsString()
  name: string

  @IsBoolean()
  @IsNotEmpty()
  is_active: boolean
}

function create() {
  const [form, setForm] = useState({
    name: '',
    is_active: true
  })

  const [loading, startLoading, stopLoading] = useLoading(false)
  const handleChange = useHandleChange<ProjectCategoryFormSchema>(setForm, form)
  const { errors, validate } = useForm()
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })
  useGuard()

  const handleSubmit = (e: any) => {
    e.preventDefault()
    startLoading()

    const schema = new ProjectCategoryFormSchema()
    schema.name = form.name
    schema.is_active = form.is_active
    const validationError = validate(schema)
    if (!validationError.length)
      return supabase
        .from('project_categories')
        .insert(form)
        .then(({ error }) => {
          stopLoading()
          if (error) {
            setIsError(true)
            setErrorObj(error)
          } else {
            setIsSuccess(true)
            router.push('/master/project')
          }
        })
    else {
      stopLoading()
      setIsError(true)
      setErrorObj({
        ...errorObj,
        message: 'Please check your data'
      } as PostgrestError)
    }
  }

  const supabase = useSupabaseClient()
  const router = useRouter()

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast severity="error" show={isError} message={errorObj.message} />

      <Toast
        severity="success"
        show={isSuccess}
        message="Data berhasil ditambahkan."
      />

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambah Data" />

          <form onSubmit={handleSubmit}>
            <ProjectCategory
              form={form}
              handleChange={handleChange}
              errors={errors}
            />
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default create
