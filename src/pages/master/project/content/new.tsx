import {
  Button,
  Card,
  CardHeader,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Switch,
  TextField
} from '@mui/material'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { decode } from 'base64-arraybuffer'
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength
} from 'class-validator'
import { useRouter } from 'next/router'
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react'
import TextArea from 'src/@core/components/text-area/TextArea'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export class ProjectContentFormSchema {
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  title: string

  @IsString()
  @IsNotEmpty()
  description: string

  @IsString()
  @IsOptional()
  location?: string

  @IsString()
  @IsNotEmpty()
  category_id: string

  @IsString()
  @IsOptional()
  service_id: string

  @IsString()
  date_completed: string

  @IsBoolean()
  is_active: boolean
}

function NewProject() {
  const [form, setForm] = useState<ProjectContentFormSchema>({
    title: '',
    category_id: '',
    date_completed: '',
    description: '',
    is_active: true,
    service_id: '',
    location: ''
  })

  const [projectPhotos, setProjectPhotos] = useState<any[]>([])

  const [projectCategories, setProjectCategories] = useState<any[]>([])
  const [services, setServices] = useState<any[]>([])
  const router = useRouter()
  const [loading, startLoading, stopLoading] = useLoading(true)
  const [isSuccess, setIsSuccess] = useState(false)
  const [isError, setIsError] = useState(false)
  const handleChange = useHandleChange<ProjectContentFormSchema>(setForm, form)
  const { errors, validate } = useForm()
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })
  useGuard()

  const fetchDeps = useCallback(
    (table_name: string) => {
      return supabase
        .from(table_name)
        .select('id, name')
        .eq('is_active', true)
        .then(({ data, error }) => {
          if (error) router.push(`/500?message=${error.message}`)

          return data
        })
    },
    [supabase]
  )

  const toBase64 = (file: File): Promise<string> =>
    new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result as string)
      reader.onerror = reject
    })

  const processPhoto = async (file: File, projectId: string) => {
    const base64 = await toBase64(file)
    const [type, _] = base64.split(';base64')
    const [__, mimetype] = type.split('data:')
    const [___, ext] = mimetype.split('/')

    const filePath = `${projectId}-${Date.now()}-photos.${ext}`

    await supabase.storage
      .from('company-profile')
      .upload(filePath, decode(base64.split('base64,')[1]), {
        contentType: mimetype
      })

    const {
      data: { publicUrl }
    } = supabase.storage
      .from('company-profile')
      .getPublicUrl(filePath, { download: true })

    console.log(publicUrl)
    return publicUrl
  }

  const createProjectPhotos = async (projectId: string): Promise<any[]> => {
    const photos = await Promise.all(
      Array.from(projectPhotos).map(async (projectPhoto) => {
        const url = await processPhoto(projectPhoto, projectId)
        return await supabase
          .from('project_photos')
          .insert({ project_id: projectId, photo: url })
      })
    )

    return photos
  }

  const submitForm = async (e: any) => {
    e.preventDefault()
    startLoading()
    try {
      const schema = new ProjectContentFormSchema()
      Object.assign(schema, form)

      const validationErrors = validate(schema)
      if (validationErrors.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
      } else {
        const newProject: any = await supabase
          .from('projects')
          .insert(schema)
          .select()
          .single()

        console.log(newProject.data)
        const photos = await createProjectPhotos(newProject.data.id)
        setIsSuccess(true)
        router.push('/master/project')
      }
    } catch (error: any) {
      console.log(error)
      setIsError(true)
      setErrorObj({
        ...error
      })
    } finally {
      stopLoading()
    }
  }

  useEffect(() => {
    fetchDeps('services')
      .then((data) => {
        setServices(data as any)
      })
      .then(() => {
        fetchDeps('project_categories').then((data) => {
          setProjectCategories(data as any)
        })
      })
      .then(() => {
        stopLoading()
      })
  }, [])

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />
      <Toast
        severity="success"
        message="Berhasil menambahkan data project"
        show={isSuccess}
      />

      <Toast severity="error" message={errorObj.message} show={isError} />

      <Grid item xs={12}>
        <form onSubmit={submitForm}>
          <Card>
            <CardHeader title="Tambah Data" />

            <Grid container spacing={6} padding={6}>
              <Grid item xs={12}>
                <TextField
                  label="Judul Project"
                  fullWidth
                  variant="outlined"
                  value={form.title}
                  onChange={handleChange('title')}
                  error={Boolean(findErrorProperty('title', errors))}
                  helperText={
                    findErrorProperty('title', errors)
                      ? findErrorProperty('title', errors)?.errors.join(', ')
                      : false
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextArea
                  value={form.description}
                  placeholder="Deskripsi Project"
                  handleChange={(e: any) => {
                    console.log(e)
                    setForm({ ...form, description: e.target.value })
                  }}
                />
              </Grid>

              <Grid container spacing={3} item xs={12}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Lokasi"
                    fullWidth
                    variant="outlined"
                    value={form.location}
                    onChange={handleChange('location')}
                    error={Boolean(findErrorProperty('location', errors))}
                    helperText={
                      findErrorProperty('location', errors)
                        ? findErrorProperty('location', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Selesai Pada Tanggal "
                    placeholder="YYYY-MM-DD, Contoh: 2023-12-30"
                    fullWidth
                    variant="outlined"
                    value={form.date_completed}
                    onChange={handleChange('date_completed')}
                    error={Boolean(findErrorProperty('date_completed', errors))}
                    helperText={
                      findErrorProperty('date_completed', errors)
                        ? findErrorProperty(
                            'date_completed',
                            errors
                          )?.errors.join(', ')
                        : false
                    }
                  />
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <label>Foto (Max 5 File)</label>
              </Grid>

              <Grid item xs={12}>
                <Button variant="contained" component="label">
                  {!Array.from(projectPhotos).length
                    ? `Tambahkan Foto`
                    : `${Array.from(projectPhotos).length} foto dipilih`}
                  <input
                    type="file"
                    max={5}
                    accept=".jpeg, .jpg, .png"
                    multiple={true}
                    hidden
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                      if (e.target.files as any) {
                        setProjectPhotos(e.target.files as any)
                      }
                    }}
                  />
                </Button>
              </Grid>

              <Grid item xs={12}>
                <label>Kategori Project</label>
              </Grid>

              <Grid item xs={12}>
                <Select
                  label="Kategori Project"
                  fullWidth
                  onChange={(e: SelectChangeEvent) =>
                    setForm({ ...form, category_id: e.target.value as string })
                  }
                >
                  {projectCategories.map((projectCategory) => (
                    <MenuItem
                      key={projectCategory.id}
                      value={projectCategory.id}
                    >
                      {projectCategory.name}
                    </MenuItem>
                  ))}
                </Select>
              </Grid>

              <Grid item xs={12}>
                <label>Service Yang Dikerjakan</label>
              </Grid>

              <Grid item xs={12}>
                <Select
                  fullWidth
                  onChange={(e: SelectChangeEvent) =>
                    setForm({ ...form, service_id: e.target.value as string })
                  }
                >
                  {services.map((service) => (
                    <MenuItem key={service.id} value={service.id}>
                      {service.name}
                    </MenuItem>
                  ))}
                </Select>
              </Grid>

              <Grid item xs={12}>
                <Grid xs={4}>
                  <label>Status</label>
                </Grid>
                <Grid>
                  <Switch
                    checked={form.is_active}
                    onChange={handleChange('is_active', !form.is_active)}
                  ></Switch>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Button type="submit" variant="contained" color="primary">
                  Simpan Data
                </Button>
              </Grid>
            </Grid>
          </Card>
        </form>
      </Grid>
    </Grid>
  )
}

export default NewProject
