import { Button, TableCell } from '@mui/material'
import { DeleteOutline, PencilBoxOutline } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import React, { useCallback, useState } from 'react'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

const columns: ITableColumn[] = [
  {
    title: 'No',
    name: 'no',
    align: 'center',
    component: ''
  },
  {
    title: 'Nama Klien',
    name: 'person_name',
    align: 'left',
    component: ''
  },
  {
    title: 'Titel',
    name: 'occupation',
    align: 'left',
    component: ''
  },
  {
    name: 'is_active',
    title: 'Status',
    align: 'center',
    component: '',
    formatter(value) {
      return value ? 'AKTIF' : 'TIDAK AKTIF'
    }
  }
]

function Testimonials() {
  const [testimonials, setTestimonials] = useState([])
  const router = useRouter()

  const fetchData = useCallback(() => {
    return supabase
      .from('testimonials')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)

        setTestimonials(data as any)
      })
  }, [supabase])
  useGuard()

  const deleteData = useCallback((selectedId) => {
    return supabase.from('testimonials').delete().eq('id', selectedId)
  }, [supabase])

  return (
    <CrudLayout
      columns={columns}
      datas={testimonials}
      deleteData={deleteData}
      fetchData={fetchData}
      title="Testimoni"
      actionChildren={(row, setOpenDialog, setSelectedId) => {
        return (
          <TableCell>
            <Button
              color="warning"
              variant="outlined"
              onClick={() => {
                router.push(`/master/testimonials/${row.id}`)
              }}
            >
              <PencilBoxOutline />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                setSelectedId(row.id)

                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default Testimonials
