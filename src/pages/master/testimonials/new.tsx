import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Switch,
  TextField
} from '@mui/material'
import { AuthError, PostgrestError } from '@supabase/supabase-js'
import { decode } from 'base64-arraybuffer'
import { IsBoolean, IsNotEmpty, IsString, MinLength } from 'class-validator'
import { useRouter } from 'next/router'
import React, { ChangeEvent, useState } from 'react'
import TextArea from 'src/@core/components/text-area/TextArea'
import { useGuard } from 'src/@core/hooks/useGuard'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useLoading } from 'src/@core/hooks/useLoading'
import { supabase } from 'src/configs/supabase'
import { findErrorProperty, useForm } from 'src/configs/validateSync'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export class TestimoniFormSchema {
  @IsString()
  @IsNotEmpty()
  person_name: string

  @IsString()
  @IsNotEmpty()
  @MinLength(10)
  body: string

  @IsString()
  @IsNotEmpty()
  occupation: string

  photo: string

  @IsNotEmpty()
  @IsBoolean()
  is_active: boolean
}

function NewTestimoni() {
  const [loading, startLoading, stopLoading] = useLoading(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [isError, setIsError] = useState(false)
  const [form, setForm] = useState<TestimoniFormSchema>({
    person_name: '',
    body: '',
    occupation: '',
    photo: '',
    is_active: true
  })
  const { errors, validate } = useForm()
  const [errorObj, setErrorObj] = useState<AuthError | PostgrestError>({
    code: '',
    details: '',
    hint: '',
    message: ''
  })
  const router = useRouter()
  const handleChange = useHandleChange(setForm, form)

  const submitForm = async (e: any) => {
    e.preventDefault()
    startLoading()

    try {
      const schema = new TestimoniFormSchema()
      Object.assign(schema, form)

      const validationErrors = validate(schema)
      if (validationErrors.length) {
        setIsError(true)
        setErrorObj({
          ...errorObj,
          message: 'Please check your data.'
        } as PostgrestError)
      } else {
        const [type, _] = form.photo.split(';base64')
        const [__, mimetype] = type.split('data:')
        const [___, ext] = mimetype.split('/')
        await supabase.storage
          .from('company-profile')
          .upload(
            `${form.person_name}-testimoni-person.${ext}`,
            decode(form.photo.split('base64,')[1]),
            {
              upsert: true,
              contentType: mimetype
            }
          )
        const {
          data: { publicUrl }
        } = supabase.storage
          .from('company-profile')
          .getPublicUrl(`${form.person_name}-testimoni-person.${ext}`, {
            download: true
          })
        await supabase
          .from('testimonials')
          .insert({ ...schema, photo: publicUrl })

        setIsSuccess(true)
        router.push(`/master/testimonials`)
      }
    } catch (error: any) {
      setIsError(true)
      setErrorObj({
        ...error
      })
    }
  }
  useGuard()

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Toast
        show={isSuccess}
        message="Data Berhasil Diterbitkan"
        severity="success"
      ></Toast>
      <Toast show={isError} message={errorObj.message} severity="error"></Toast>

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Tambah Testimoni" />

          <form onSubmit={submitForm}>
            <CardContent>
              <Grid container spacing={6}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Nama Klien"
                    value={form.person_name}
                    error={Boolean(findErrorProperty('person_name', errors))}
                    helperText={
                      findErrorProperty('person_name', errors)
                        ? findErrorProperty('person_name', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                    onChange={handleChange('person_name')}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Titel"
                    value={form.occupation}
                    error={Boolean(findErrorProperty('occupation', errors))}
                    helperText={
                      findErrorProperty('occupation', errors)
                        ? findErrorProperty('occupation', errors)?.errors.join(
                            ', '
                          )
                        : false
                    }
                    onChange={handleChange('occupation')}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextArea
                    value={form.body}
                    placeholder="Isi Testimoni"
                    handleChange={(e: any) =>
                      setForm({ ...form, body: e.target.value })
                    }
                  ></TextArea>
                </Grid>

                <Grid item xs={12}>
                  <label>Foto Klien</label>
                </Grid>

                <Grid item xs={12}>
                  <Button component="label" variant="contained" color="primary">
                    Tambahkan Foto
                    <input
                      type="file"
                      max={5}
                      accept=".jpeg, .jpg, .png"
                      multiple={true}
                      hidden
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        if ((e.target.files as any)[0]) {
                          const reader = new FileReader()
                          const file = (e.target.files as any)[0]
                          reader.onloadend = () => {
                            setForm({ ...form, photo: reader.result as any })
                          }
                          reader.readAsDataURL(file)
                        }
                      }}
                    />
                  </Button>
                </Grid>

                <Grid item xs={12}>
                  <label>Status</label>
                </Grid>

                <Grid item xs={12}>
                  <Switch
                    checked={form.is_active}
                    onChange={handleChange('is_active')}
                  ></Switch>
                </Grid>

                <Grid item xs={12} alignContent="end">
                  <Button type="submit" variant="contained" color="primary">
                    Simpan
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default NewTestimoni
