import { Button, TableCell } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { DeleteOutline, Pencil } from 'mdi-material-ui'
import router from 'next/router'
import React, { useCallback, useState } from 'react'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

function TabCategory() {
  const supabase = useSupabaseClient()
  const fetchCategory = useCallback(() => {
    return supabase
      .from('project_categories')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)

        setCategories(data as any)
      })
  }, [supabase])

  const deleteCategory = (selectedId: any) => {
    return supabase.from('project_categories').delete().eq('id', selectedId)
  }

  const [categories, setCategories] = useState([])

  const columns: ITableColumn[] = [
    {
      name: 'no',
      title: 'No',
      align: 'left',
      component: ''
    },
    {
      name: 'name',
      title: 'Nama Kategori',
      align: 'left',
      component: ''
    },
    {
      name: 'is_active',
      title: 'Status',
      align: 'left',
      component: '',
      formatter(value) {
        return value ? 'AKTIF' : 'TIDAK AKTIF'
      }
    },
    {
      name: 'action',
      title: 'Aksi',
      align: 'center',
      component: ''
    }
  ]

  return (
    <CrudLayout
      columns={columns}
      datas={categories}
      fetchData={fetchCategory}
      title="Kategori Project"
      deleteData={deleteCategory}
      addUrl="/master/project/category/new"
      actionChildren={(row, setOpenDialog, setSelectedId) => {
        return (
          <TableCell align="center">
            <Button
              style={{ marginRight: '1rem' }}
              color="warning"
              variant="outlined"
              onClick={() =>
                router.push(`/master/project/category/edit/${row.id}`)
              }
            >
              <Pencil />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                setSelectedId(row.id)
                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default TabCategory
