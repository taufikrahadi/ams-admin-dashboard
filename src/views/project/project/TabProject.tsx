import { Button, TableCell } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { DeleteOutline, Pencil } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import React, { useCallback, useEffect, useState } from 'react'
import CrudLayout, { ITableColumn } from 'src/layouts/CrudLayout'

const dayjs = require('dayjs')

const column: ITableColumn[] = [
  {
    name: 'no',
    title: 'NO',
    component: '',
    align: 'center'
  },
  {
    name: 'title',
    title: 'Judul',
    component: '',
    align: 'center'
  },
  {
    name: 'created_at',
    title: 'Dibuat Pada',
    component: '',
    formatter(value) {
      return dayjs(value).format('YYYY-MM-DD')
    },
    align: 'center'
  }
]

function TabProject() {
  const [projects, setProjects] = useState([])
  const supabase = useSupabaseClient()
  const router = useRouter()

  const fetchProject = useCallback(() => {
    return supabase
      .from('projects')
      .select('*')
      .then(({ data, error }) => {
        if (error) router.push(`/500?message=${error.message}`)

        setProjects(data as any)
      })
  }, [supabase])

  const deleteProject = (selectedId: any) => {
    return supabase.from('projects').delete().eq('id', selectedId)
  }

  return (
    <CrudLayout
      columns={column}
      datas={projects}
      fetchData={fetchProject}
      deleteData={deleteProject}
      title="Project"
      addUrl="/master/project/content/new"
      actionChildren={(row, setOpenDialog, setSelectedId) => {
        return (
          <TableCell align="center">
            <Button
              style={{ marginRight: '1rem' }}
              color="warning"
              variant="outlined"
              onClick={() =>
                router.push(`/master/project/content/edit/${row.id}`)
              }
            >
              <Pencil />
            </Button>
            <Button
              color="error"
              variant="outlined"
              onClick={() => {
                setSelectedId(row.id)
                setOpenDialog(true)
              }}
            >
              <DeleteOutline />
            </Button>
          </TableCell>
        )
      }}
    />
  )
}

export default TabProject
