import { Button, Grid, Switch, TextField } from '@mui/material'
import React from 'react'
import {
  FormValidationError,
  findErrorProperty
} from 'src/configs/validateSync'
import { ProjectCategoryFormSchema } from 'src/pages/master/project/category/new'

export interface ProjectCategoryFormProps {
  form: ProjectCategoryFormSchema
  errors: FormValidationError[]
  handleChange(prop: string, value?: any): any
}

function ProjectCategory(props: ProjectCategoryFormProps) {
  return (
    <Grid container padding={6} spacing={6}>
      <Grid item xs={12}>
        <TextField
          label="Nama Kategori"
          fullWidth
          variant="outlined"
          value={props.form.name}
          onChange={props.handleChange('name')}
          error={Boolean(findErrorProperty('name', props.errors))}
          helperText={
            findErrorProperty('name', props.errors)
              ? findErrorProperty('name', props.errors)?.errors.join(', ')
              : false
          }
        />
      </Grid>

      <Grid item xs={12}>
        <Grid xs={4}>
          <label>Status</label>
        </Grid>
        <Grid>
          <Switch
            checked={props.form.is_active}
            onChange={props.handleChange('is_active', !props.form.is_active)}
          ></Switch>
        </Grid>
      </Grid>

      <Grid item xs={12} alignItems={'end'}>
        <Button type="submit" variant={'contained'}>
          Submit
        </Button>
      </Grid>
    </Grid>
  )
}

export default ProjectCategory
