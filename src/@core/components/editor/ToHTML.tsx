import { OutputData } from '@editorjs/editorjs'
import { Grid } from '@mui/material'
import React from 'react'

const editorJsHTML = require('editorjs-html')
const EditorJsToHTML = editorJsHTML()

type Props = {
  data: OutputData
}
type ParsedContent = string | JSX.Element

function ToHTML(props: Props) {
  const html = EditorJsToHTML.parse(props.data) as ParsedContent[]
  return (
    <Grid container key={props.data.time}>
      {html.map((item, index) => {
        if (typeof item === 'string') {
          return (
            <div dangerouslySetInnerHTML={{ __html: item }} key={index}></div>
          )
        }
        return item
      })}
    </Grid>
  )
}

export default ToHTML
