//./components/Editor
import React, { memo, useEffect, useRef } from 'react'
import EditorJS, { OutputData } from '@editorjs/editorjs'
import { Grid } from '@mui/material'
import { ConsoleLine } from 'mdi-material-ui'

const Paragraph = require('@editorjs/paragraph')
const Header = require('@editorjs/header')
const Image = require('@editorjs/image')
const Link = require('@editorjs/link')
const Checklist = require('@editorjs/checklist')

//props
type Props = {
  data?: OutputData
  onChange(val: OutputData): void
  holder: string
}

const EDITOR_TOOLS = {
  paragraph: Paragraph,
  header: Header,
  link: Link,
  checklist: Checklist
}

const EditorBlock = ({ data, onChange, holder }: Props) => {
  //add a reference to editor
  const ref = useRef<EditorJS>()

  //initialize editorjs
  useEffect(() => {
    //initialize editor if we don't have a reference

    if (!ref.current) {
      const editor = new EditorJS({
        holder: holder,
        tools: {
          ...EDITOR_TOOLS,
          image: {
            class: Image,
            config: {
              uploader: {
                async uploadByFile(file: any) {}
              }
            }
          }
        },
        data,
        async onChange(api, event) {
          const data = await api.saver.save()
          console.log(data)

          onChange(data)
        }
      })
      ref.current = editor
    }

    //add a return function handle cleanup
    return () => {
      if (ref.current && ref.current.destroy) {
        ref.current.destroy()
      }
    }
  }, [data])

  return (
    <Grid container>
      <Grid item xs={12}>
        <div id={holder}></div>
      </Grid>
    </Grid>
  )
}

export default memo(EditorBlock)
