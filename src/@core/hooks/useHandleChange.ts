import { isNotEmpty } from 'class-validator'
import { Dispatch } from 'react'

export const useHandleChange =
  <T>(setForm: Dispatch<any>, form: T) =>
  (prop: keyof T, value?: any) =>
  (e: any) => {
    console.log(value)

    setForm({
      ...form,
      [prop]: isNotEmpty(value) ? value : e.target.value
    })
  }
