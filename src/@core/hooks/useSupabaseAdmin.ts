import { SupabaseClient } from '@supabase/supabase-js'
import { useMemo } from 'react'

export const useSupabaseAdmin = () =>
  useMemo(
    () =>
      new SupabaseClient(
        String(process.env.NEXT_PUBLIC_SUPABASE_URL),
        String(process.env.NEXT_PUBLIC_SUPABASE_ROLE_KEY)
      ),
    []
  )
