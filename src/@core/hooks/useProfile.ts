import {
  useSupabaseClient,
  useUser
} from '@supabase/auth-helpers-react'
import nProgress from 'nprogress'
import { useEffect } from 'react'

export const useProfile = (setter: any) => {
  const supabase = useSupabaseClient()
  const user = useUser()

  useEffect(() => {
    ;(async function () {
      nProgress.start()
      if (user) {
        const { data } = await supabase
          .from('profiles')
          .select('*, roles (name)')
          .eq('id', user?.id)

        setter(data)
        nProgress.done()
      }
    })()
  }, [user])
}
