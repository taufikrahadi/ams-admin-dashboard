import {
  Button,
  Card,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@mui/material'
import { isNotEmpty } from 'class-validator'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useLoading } from 'src/@core/hooks/useLoading'
import ConfirmDialog from 'src/views/ConfirmDialog'
import Loading from 'src/views/Loading'
import Toast from 'src/views/Toast'

export interface ITableColumn {
  name: string
  child?: string
  title: string
  component: any
  formatter?(value: any): any
  align: 'left' | 'center' | 'right' | 'inherit' | 'justify'
}

export interface CrudLayoutProps {
  fetchData(): Promise<any> | any
  deleteData(id: string): Promise<any> | any
  datas: any[]
  columns: ITableColumn[]
  title: string
  actionChildren?(row: { [k: string]: any }, setOpenDialog: any, setSelectedId: any): any
  addUrl?: string
}

const CrudLayout = (props: CrudLayoutProps) => {
  const router = useRouter()
  const [loading, startLoading, stopLoading] = useLoading(true)
  const [successDeletion, setSuccessDeletion] = useState(false)
  const [openDialog, setOpenDialog] = useState(false)

  const handleDelete = () => {
    startLoading()
    props.deleteData(selectedId as any).then(() => {
      props.fetchData().then(() => {
        setSuccessDeletion(true)
        setOpenDialog(false)
        stopLoading()
      })
    })
  }

  const [selectedId, setSelectedId] = useState<string | number>('')

  const implementRowColumn = (
    row: { [k: string]: any },
    column: ITableColumn
  ) => {
    let result = ''

    if ('child' in column) {
      result = row[column.name][column.child as string]
      if ('formatter' in column) {
        result = column.formatter?.(result)
      }
    } else {
      result = row[column.name]
      if ('formatter' in column) {
        result = column.formatter?.(result)
      }
    }

    return result
  }

  useEffect(() => {
    startLoading()
    props.fetchData().then(() => stopLoading())
  }, [])

  return (
    <>
      {/* Loading & Backdrop */}
      <Loading show={loading} />

      {/* Success Delete Toast */}
      <Toast
        message="Success deleting data"
        severity="success"
        show={successDeletion}
        onClose={() => setSuccessDeletion(false)}
      />

      <Grid container spacing={6}>
        <Grid item xs={12}>
          <Typography variant="h5">{props.title}</Typography>
        </Grid>

        <Grid item xs={12}>
          <Card>
            {/* Add Data Button */}
            <Grid container paddingY={5} paddingX={2} spacing={6}>
              <Grid item md={2} xs={6}>
                <Button
                  fullWidth
                  size="small"
                  color="primary"
                  variant="contained"
                  onClick={() =>
                    isNotEmpty(props.addUrl)
                      ? router.push(String(props.addUrl))
                      : router.push(router.pathname + '/new')
                  }
                >
                  Add New Data
                </Button>
              </Grid>
            </Grid>

            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }}>
                <TableHead>
                  <TableRow>
                    {props.columns.map((column) => (
                      <TableCell
                        align={column.align as any}
                        key={column.name}
                        component={column.component as any}
                      >
                        {column.title}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>

                <TableBody>
                  {props.datas.map((data: any, index: number) => (
                    <TableRow key={index}>
                      <TableCell component="th" align="left" scope="row">
                        {index + 1}
                      </TableCell>
                      {props.columns.map((column) => {
                        if (column.name !== 'no' && column.name !== 'action')
                          return (
                            <TableCell
                              key={column.name}
                              component={column.component}
                              align={column.align}
                            >
                              {implementRowColumn(data, column)}
                            </TableCell>
                          )
                      })}
                      {props.actionChildren ? (
                        props.actionChildren(data, setOpenDialog, setSelectedId)
                      ) : (
                        <></>
                      )}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Card>
        </Grid>
      </Grid>

      {/* Confirm Delete Dialog */}
      <ConfirmDialog
        title="Konfirmasi Hapus Data"
        message="Data yang sudah dihapus tidak bisa dikembalikan lagi."
        show={openDialog}
        setShow={setOpenDialog}
        action={handleDelete}
      />
    </>
  )
}

export default CrudLayout
