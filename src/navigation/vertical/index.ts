// ** Icon imports
import Login from 'mdi-material-ui/Login'
import Table from 'mdi-material-ui/Table'
import CubeOutline from 'mdi-material-ui/CubeOutline'
import HomeOutline from 'mdi-material-ui/HomeOutline'
import FormatLetterCase from 'mdi-material-ui/FormatLetterCase'
import AccountCogOutline from 'mdi-material-ui/AccountCogOutline'
import CreditCardOutline from 'mdi-material-ui/CreditCardOutline'
import AccountPlusOutline from 'mdi-material-ui/AccountPlusOutline'
import AlertCircleOutline from 'mdi-material-ui/AlertCircleOutline'
import GoogleCirclesExtended from 'mdi-material-ui/GoogleCirclesExtended'

// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: 'Dashboard',
      icon: HomeOutline,
      path: '/'
    },
    {
      title: 'App Settings',
      icon: AccountCogOutline,
      path: '/master/app-setting'
    },
    {
      sectionTitle: 'Master Data'
    },
    {
      title: 'Services',
      icon: Login,
      path: '/master/services'
    },
    {
      title: 'Articles',
      icon: FormatLetterCase,
      path: '/master/articles'
    },
    {
      title: 'Teams',
      icon: AccountPlusOutline,
      path: '/master/teams'
    },
    {
      title: 'Projects',
      icon: AlertCircleOutline,
      path: '/master/project'
    },
    {
      title: 'Client',
      icon: AlertCircleOutline,
      path: '/master/client'
    },
    {
      title: 'Testimonial',
      icon: AlertCircleOutline,
      path: '/master/testimonials'
    },
    {
      title: 'News',
      icon: GoogleCirclesExtended,
      path: '/master/news'
    }
  ]
}

export default navigation
