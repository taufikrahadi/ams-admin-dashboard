import { plainToClass, plainToInstance } from 'class-transformer'
import { validateSync } from 'class-validator'
import { useState } from 'react'

export type FormValidationError = {
  property: string
  errors: string[]
}

export const findErrorProperty = (
  name: string,
  errors: FormValidationError[]
) => {
  return errors.find((error) => error.property === name)
}

export const validate = (schema: any): FormValidationError[] => {
  const validate = validateSync(schema)

  if (validate.length > 0) {
    const errors = validate.map((error) => {
      const { property, constraints } = error

      const keys = Object.keys(constraints as any)

      const msgs: string[] = []

      keys.forEach((key) => {
        msgs.push(`${constraints?.[key]}`)
      })

      return {
        property,
        errors: msgs
      }
    })

    return errors as FormValidationError[]
  } else return []
}

export const useForm = () => {
  const [errors, setErrors] = useState<any[]>([])

  const validate = (value: any) => {
    const validate = validateSync(value)

    if (validate.length > 0) {
      const errors = validate.map((error) => {
        const { property, constraints } = error

        const keys = Object.keys(constraints as any)

        const msgs: string[] = []

        keys.forEach((key) => {
          msgs.push(`${constraints?.[key]}`)
        })

        return {
          property,
          errors: msgs
        }
      })

      setErrors(errors)

      return errors as FormValidationError[]
    } else return []
  }

  return {
    errors,
    validate
  }
}
